#include <pthread.h>


void *value()
{
	return (void *) 42;
}

long int returnValue()
{
	pthread_t tid;
	void *status;

	pthread_create(&tid, NULL, value, NULL);
	pthread_join(tid, &status);

	

	return (long int)status;
}

