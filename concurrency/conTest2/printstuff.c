#include <stdio.h>
#include "conTest2.c"
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "threadID.c"

int main(void)
{
	long int li = returnValue();
	printf("print value %ld\n", &li);

	char* s = tGet();
	printf("print thread id %s\n", s);

	return 0;
}
