#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>


void* stuff(void *p)
{
	int i = *(int *)p;
	for(int n = 0; n <= i; n++)
	{
		printf("%i\n", n);
	}
}

void* moreStuff(void *p)
{
	int i = *(int *)p;
	while(i < 20)
	{
		printf("%i\n", i);
		i = i+2;
	}
}

int main(void)
{
	int p = scanf("Enter number %i\n", &p);
	pthread_t t1, t2;
	
	pthread_create(&t1, NULL, stuff, (void *)&p);	
	pthread_create(&t2, NULL, moreStuff, (void *)&p);
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);

	return 0;
}
