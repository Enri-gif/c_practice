#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

void * printtid(void * tid)
{
	long * id = (long *) tid;
	printf("Thread ID is %ld\n", *id);
}

int main()
{
	pthread_t tid;
	pthread_t tid1;

	pthread_create(&tid, NULL, printtid, (void *)&tid);


	pthread_create(&tid1, NULL, printtid, (void *)&tid1);
	
	pthread_exit(NULL);
	return 0;
}
