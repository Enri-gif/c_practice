#include <stdio.h>
#include "..\adjacencyMatrix\adjacencyMatrix.c"

//størelsen af matrixen
#define MAXL 225
#define MAXB 225
//hvis matrix er 0 kosten er uendelig
#define INF 9999

void dij(int graf[MAXL][MAXB], int n, int start);
void dij(int graf[MAXL][MAXB], int n, int start)
{
	int cost[MAXL][MAXB], distance[MAXL], pred[MAXB];
	int visited[MAXL], count, mindistance, nextnode, i, j;
	//cost matrix
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
		{
			if(graf[i][j] == 0)
			{
				cost[i][j] = INF;
			}
			else
			{
				cost[i][j] = graf[i][j];
			}
		}
	}
	//bruger cost matrix til at udregne distance mellem start of i
	for(i = 0; i < n; i++)
	{
		distance[i] = cost[start][i];
		pred[i] = start;
		visited[i] = 0;
	}
	//sætter start punkt
	distance[start] = 0;
	visited[start] = 1;
	count = 1;
	
	while(count < n - 1)
	{
	//	printf("\n%d", nextnode);
		mindistance = INF;
		//tjekker n mængde nodes og ser hvis det er mulig at gå fra start node til n node
		//og visited[] skal være 0 så den tæller en node 1 gang
		for(i = 0; i < n; i++)
		{
			//i = 4
			if(distance[i] < mindistance && !visited[i])
			{
				mindistance = distance[i];
				nextnode = i;
			}
		}
		visited[nextnode] = 1;
		//tjekker distance mellem den næste node og næste igen
		//med hjælp af den tidligere for loop
		for(i = 0; i < n; i++)
		{	
			if(!visited[i])
			{
				if(mindistance + cost[nextnode][i] < distance[i])
				{
					distance[i] = mindistance + cost[nextnode][i];
					pred[i] = nextnode;
				}
			}
		}
		count++;
	}

	//print distance

	for (i = 0; i < n; i++)
	    if (i != start && i == n-1)
	    {
	          printf("\nDistance from %d to %d: %d", start, i, distance[i]);
	    }
/*
	for (i = 0; i < n; i++)
		    if (i != start) {
			          printf("\nDistance from source to %d: %d", i, distance[i]);
				      }
*/
}

int main(void)
{
	int graf[MAXL][MAXB];
	int i, j, n, u;

	printf("indsæt start node: ");
	scanf("%d", &u);
	printf("indsæt slut node: ");
	scanf("%d", &n);
	if(u > n)
	{
		n = u+n;
		u = n-u;
		n = n-u;
	}
	init(graf);

	

	//printMatrix(graf);
	//printf("32 til 11 %d", graf[32][11]);
	n++;
	dij(graf, n, u);
    return 0;
}
