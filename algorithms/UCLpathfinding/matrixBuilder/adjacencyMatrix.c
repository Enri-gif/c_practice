#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int Size, length;

// initialize to 1
int** init(int Size, int length)
{
	//Size er antal nodes
	//length er sidelængde af matrixen
	int i, j;
	int* temp = calloc(Size*length, sizeof(int));
	int** graf = malloc(Size*sizeof(int*));
	for(i = 0; i < Size; i++)
	{
		graf[i] = temp + i*length;
	}
	for(i = 0; i < Size; i++)
	{
		for(j = 0; j < Size; j++)
		{
			if (i < Size && i == j-length || i == j+length || i == j-1 || i == j+1)
				graf[i][j] = 1;
			//for unenven matrix
			else
				graf[i][j] = 0;
		}
	}
	return graf;
}

//matrix edges
void removeEdge(int** graf, int i, int j)
{
	graf[i][j] = 0;
	graf[j][i] = 0;
}

//print matrix
void printMatrix(int** graf)
{
	int i, j;
	int arr[i];
	int arr3[j];
	int arr2[i][j];
	//hardcoded størrelse fordi matrix er for stor
	
	for(i = 0; i < Size; i++)
	{
		arr[i] = *(*graf + i);
		//printf("%d ", i);
		for(j = 0; j < Size; j++)
		{
			arr3[j] = *(*graf + j);
//			printf("%d", graf[i][j]);
		}
//		printf("\n");
	}
	for(i = 0; i < Size; i++)
	{
		for(j = 0; j < Size; j++)
		{
			printf("%d %d", arr[i], arr3[j]);
		}
		printf("\n");
	}
}

int main(void)
{
	printf("indsæt antal nodes: ");
	scanf("%d", &Size);
	printf("\nindsæt sidelængden: ");
	scanf("%d", &length);
//	init(Size, length);
	int* test = *init(Size, length);
	printMatrix(&test);
	return 0;
}

