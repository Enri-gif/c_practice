#include <stdio.h>

int Size = 750;
int length = 15;

// initialize to 1
void init(int graf[Size][Size])
{
	//Size er antal nodes
	//length er sidelængde af matrixen
	int i, j;
//	int graf[Size][Size];
/*	int* temp = calloc(Size*length, sizeof(int));
	int** graf = malloc(Size*sizeof(int*));
	for(i = 0; i < Size; i++)
	{
		graf[i] = temp + i*length;
	}
*/	for(i = 0; i < Size; i++)
	{
		for(j = 0; j < Size; j++)
		{
			if (i < Size)
			{
				if(i == j-length || i == j+length || i == j-1 || i == j+1)
				{
					if(j%15 == 0 && i == j-1)
					{
						graf[i][j] = 0;
					}
					else
						graf[i][j] = 1;
				}
				else
					graf[i][j] = 0;
			}
			//for unenven matrix
			else
				graf[i][j] = 0;
		}
	}
/*	
	for(i = 0; i < Size; i++)
	{
		//printf("%d ", i);
		for(j = 0; j < Size; j++)
		{
			printf("%d ", graf[i][j]);
		}
		printf("\n");
	}
*/	
}

//matrix edges
void removeEdge(int** graf, int i, int j)
{
	graf[i][j] = 0;
	graf[j][i] = 0;
}

//print matrix
void printMatrix(int** graf)
{
	int i, j;
	//hardcoded størrelse fordi matrix er for stor
	for(i = 0; i < Size; i++)
	{
		//printf("%d ", i);
		for(j = 0; j < Size; j++)
		{
			printf("%d ", graf[i][j]);
		}
		printf("\n");
	}
}
/*
int main(void)
{
//	printf("indsæt antal nodes");
//	scanf("%d", &Size);
//	printf("indsæt sidelængde");
//	scanf("%d", &length);
	init(Size, length);
//	printMatrix(init(Size, length));
	return 0;
}
*/
