#include <stdio.h>
//størelsen af matrixen
#define MAXL 225
#define MAXB 225
//hvis matrix er 0 kosten er uendelig
#define INF 9999

void init(int arr[MAXL][MAXB])
{
	int i, j;
	for(i = 0; i < MAXL; i++)
	{
		for(j = 0; j < MAXL; j++)
		{
			
			if (i < MAXL && i == j-21 || i == j+21 || i == j-1 || i == j+1)
			{
				arr[i][j] = 1;
			}//for unenven matrix
			else
				arr[i][j] = 0;
		}
	}	
}

//matrix edges
void removeEdge(int arr[MAXL][MAXB], int i, int j)
{
	arr[i][j] = 0;
	arr[j][i] = 0;
}

//print matrix
void printMatrix(int arr[MAXL][MAXB])
{
	int i, j;
	//hardcoded størrelse for matrix er for stor
	for(i = 0; i < 30; i++)
	{
		//printf("%d ", i);
		for(j = 0; j < MAXB; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}


void dij(int graf[MAXL][MAXB], int n, int start);
void dij(int graf[MAXL][MAXB], int n, int start)
{
	int cost[MAXL][MAXB], distance[MAXL], pred[MAXB];
	int visited[MAXL], count, mindistance, nextnode, i, j;
	//cost matrix
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
		{
			if(graf[i][j] == 0)
			{
				cost[i][j] = INF;
			}
			else
			{
				cost[i][j] = graf[i][j];
			}
		}
	}
	//bruger cost matrix til at udregne distance mellem start of i
	for(i = 0; i < n; i++)
	{
		distance[i] = cost[start][i];
		pred[i] = start;
		visited[i] = 0;
	}
	//sætter start punkt
	distance[start] = 0;
	visited[start] = 1;
	count = 1;
	
	while(count < n - 1)
	{
		mindistance = INF;
		for(i = 0; i < n; i++)
		{
			if(distance[i] < mindistance && !visited[i])
			{
				mindistance = distance[i];
				nextnode = i;
			}
		}
		
		visited[nextnode] = 1;
		printf("\n%d", nextnode);
		for(i = 0; i < n; i++)
		{	
			if(!visited[i])
			{
				if(mindistance + cost[nextnode][i] < distance[i])
				{
					distance[i] = mindistance + cost[nextnode][i];
					pred[i] = nextnode;
				}
			}
		}
		count++;
	}
/*
	//print distance
	for(i = 0; i < n; i++)
	{
		if(i != start)
			printf("\nDistance from source to %d: %d", i, distance[i]);
	}
	//print path

	for(i = 0; i < MAXBof(path)/MAXBof(path[0]); i++)
	{
		printf("\npath: %d", path[i]);
	}*/
}

int main(void)
{
	int graf[MAXL][MAXB];
	int i, j, n, u;
	n = 42;
	u = 0;
	init(graf);

	

	//printMatrix(graf);
	printf("1 til 2 %d", graf[1][2]);

	dij(graf, n, u);
    return 0;
}
