#include <stdio.h>

int main(void)
{
	int arr[9] = {1,2,3,4,5,6,7,8,9};
	
	int arrSize = sizeof(arr)/sizeof(arr[0]);

	int low = arr[0];
	int high = arr[8];
	int mid;
	int target;
	scanf("%i", &target);
//	printf("low = %i\nmid = %i\nhigh = %i\ntarget = %i\n", low, mid, high, target);

do{

	mid = arr[(low + high)/2];
	if(target == mid)
	{
		return mid;
	}
	else if(target > mid)
	{
		low = mid+1;
	}
	else if(target < mid)
	{
		high = mid-1;
	}
	
	printf("searching...\nmid is %i\ntarget is %i\n", mid, target);
}while(target != mid);
/*
	for (int i = 0; i != arrSize; i++)
	{
		printf("arr: %i\n", i);
	}
*/	

	//printf("size of arr = %i", arrSize);
	return 0;
}
