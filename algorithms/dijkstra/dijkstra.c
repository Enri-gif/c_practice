#include <stdio.h>

#define INF 9999
#define MAX 10

void dij(int graf[MAX][MAX], int n, int start);

void dij(int graf[MAX][MAX], int n, int start)
{
	int cost[MAX][MAX], distance[MAX], pred[MAX];
	int visited[MAX], count, mindistance, nextnode, i, j;

	//cost matrix
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
		{
			if(graf[i][j] == 0)
			{
				cost[i][j] = INF;
			}
			else
			{
				cost[i][j] = graf[i][j];
			}
		}
	}

	for(i = 0; i < n; i++)
	{
		distance[i] = cost[start][i];
		pred[i] = start;
		visited[i] = 0;
	}

	distance[start] = 0;
	visited[start] = 1;
	count = 1;

	while(count < n - 1)
	{
		mindistance = INF;
		for(i = 0; i < n; i++)
		{
			if(distance[i] < mindistance && !visited[i])
			{
				mindistance = distance[i];
				nextnode = i;
			}
		}
		
		visited[nextnode] = 1;
		for(i = 0; i < n; i++)
		{	
			if(!visited[i])
			{
				if(mindistance + cost[nextnode][i] < distance[i])
				{
					distance[i] = mindistance + cost[nextnode][i];
					pred[i] = nextnode;
				}
			}
		}
		count++;
	}

	//print distance
	for(i = 0; i < n; i++)
	{
		if(i != start)
			printf("\nDistance from source to %d: %d", i, distance[i]);
	}
}

int main(void)
{
	int graf[MAX][MAX], i, j, n, u;
	n = 7;

graf[0][0] = 0;
graf[0][1] = 0;
graf[0][2] = 1;
graf[0][3] = 2;
graf[0][4] = 0;
graf[0][5] = 0;
graf[0][6] = 0;

graf[1][0] = 0;
graf[1][1] = 0;
graf[1][2] = 2;
graf[1][3] = 0;
graf[1][4] = 0;
graf[1][5] = 3;
graf[1][6] = 0;

graf[2][0] = 1;
graf[2][1] = 2;
graf[2][2] = 0;
graf[2][3] = 1;
graf[2][4] = 3;
graf[2][5] = 0;
graf[2][6] = 0;

graf[3][0] = 2;
graf[3][1] = 0;
graf[3][2] = 1;
graf[3][3] = 0;
graf[3][4] = 0;
graf[3][5] = 0;
graf[3][6] = 1;

graf[4][0] = 0;
graf[4][1] = 0;
graf[4][2] = 3;
graf[4][3] = 0;
graf[4][4] = 0;
graf[4][5] = 2;
graf[4][6] = 0;

graf[5][0] = 0;
graf[5][1] = 3;
graf[5][2] = 0;
graf[5][3] = 0;
graf[5][4] = 2;
graf[5][5] = 0;
graf[5][6] = 1;

graf[6][0] = 0;
graf[6][1] = 0;
graf[6][2] = 0;
graf[6][3] = 1;
graf[6][4] = 0;
graf[6][5] = 1;
graf[6][6] = 0;

u = 0; dij(graf, n, u);
	

	return 0;
}
